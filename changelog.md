# 0.2.0

Updates needed to make implemented methods work with new version of AgoraDesk / LocalMonero API (v1.6)

Also started using [Python Poetry](https://python-poetry.org/) for dependencies / requirements control.

# 0.1.0

Initial release. Following API calls have been **not** implemented:
- /trade/contact_release/{trade_id} • Release trade escrow
- /contact_fund/{trade_id} • Fund a trade
- /contact_dispute/{trade_id} • Start a trade dispute
- /contact_escrow/{trade_id} • Enable escrow
- Image uploading in chat
- /contact_message_attachment/{trade_id}/{attachment_id} • Get a trade chat attachment
